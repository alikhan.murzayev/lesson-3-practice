package main

import "fmt"

func printStack(stack *[]int32) {
	for i := 0; i < len(*stack); i++ {
		fmt.Print((*stack)[i], " ")
	}
	fmt.Println()
}

func push(stack *[]int32, stackSize *int32, value int32) {
	if int32(len(*stack)) >= *stackSize {
		fmt.Println("error: stack overflow")
		return
	}

	*stack = append(*stack, value)

}

func pop(stack *[]int32) {
	if int32(len(*stack)) == 0 {
		fmt.Println("error: empty stack")
		return
	}
	*stack = (*stack)[:len(*stack)-1]
}

func top(stack *[]int32) int32 {
	if int32(len(*stack)) == 0 {
		fmt.Println("error: empty stack")
		return 0
	}
	return (*stack)[len(*stack) - 1]
}

func getMax(stack *[]int32) int32 {
	if int32(len(*stack)) == 0 {
		fmt.Println("error: empty stack")
	}
	max := (*stack)[0]
	for i := 1; i < len(*stack); i++ {
		if (*stack)[i] > max {
			max = (*stack)[i]
		}
	}
	return max
}

func initStack(stack *[]int32, stackSizeVar *int32, initValues *[]int32, stackSize int32) {
	copy(*stack, *initValues)
	*stackSizeVar = stackSize
}

func main() {
	// stack that stores stack elements
	var stack []int32
	var stackSize int32

	initStack(&stack, &stackSize, []int32{2, 6, 8, 4, 3, 6, 3}, 15)

	fmt.Println("Initial stack:")
	printStack(&stack)

	push(&stack, &stackSize,  4)
	push(&stack, &stackSize, 5)
	fmt.Println("After push 4 and 5")
	printStack(&stack)

	fmt.Println("Max value:")
	fmt.Println(getMax(&stack))

	fmt.Println("Last element:")
	fmt.Println(top(&stack))

	pop(&stack)
	pop(&stack)
	pop(&stack)
	pop(&stack)
	pop(&stack)
	pop(&stack)
	fmt.Println("pop 6 times")
	printStack(&stack)
}